package com.example.jack_sparrow.imagefetch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.speech.tts.Voice;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    public final String[] urlToGo = {"http://www.readersdigest.ca/wp-content/uploads/2011/01/4-ways-cheer-up-depressed-cat.jpg"
    ,"https://www.petfinder.com/wp-content/uploads/2013/09/cat-black-superstitious-fcs-cat-myths-162286659.jpg"
    ,"https://www.rd.com/wp-content/uploads/sites/2/2016/04/01-cat-wants-to-tell-you-laptop.jpg"
    ,"https://i.kinja-img.com/gawker-media/image/upload/s--kx3d-Wgg--/c_scale,fl_progressive,q_80,w_800/ol9ceoqxidudap8owlwn.jpg"
    ,"https://i.pinimg.com/736x/6f/83/80/6f8380ea1a15f6a9f666273ba8475728--cute-rottweiler-puppy-rotweiler-puppies.jpg"};

    int imageCounter =0;

    Button button1;
    Button prev;
    Button next;

    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = (Button) findViewById(R.id.button);
        image = (ImageView) findViewById(R.id.img);
        prev = (Button) findViewById(R.id.prev);
        next = (Button) findViewById(R.id.next);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FetchImage().execute();
                showFiles(imageCounter);
                button1.setVisibility(View.GONE);
                next.setVisibility(View.VISIBLE);

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(imageCounter<4){
                    imageCounter++;
                    showFiles(imageCounter);
                    if(prev.getVisibility()==View.INVISIBLE){
                        prev.setVisibility(View.VISIBLE);
                    }
                }
                if(imageCounter==4){
                    next.setVisibility(View.INVISIBLE);
                }
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageCounter>0){
                    imageCounter--;
                    showFiles(imageCounter);
                    if(next.getVisibility()==View.INVISIBLE){
                        next.setVisibility(View.VISIBLE);
                    }
                }
                if(imageCounter==0){
                    prev.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public class FetchImage extends AsyncTask<Void, Void, Void>{

        int i = 0;

        @Override
        protected Void doInBackground(Void... voids) {



            try {
                for(i=0;i<5;i++){
                    int mConnectionCode;

                    URL url = new URL(urlToGo[i]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);

                    mConnectionCode = connection.getResponseCode();

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (mConnectionCode == HttpURLConnection.HTTP_OK) {

                        InputStream inputStream = connection.getInputStream();

                        Log.v("TAG", "Image"+i+" fetched");

                        writeFileToInternalMemory("img"+i+".png", inputStream);


                }

                }
            } catch (IOException e) {
                Log.v("Message", e.getMessage());
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void writeFileToInternalMemory(String filename, InputStream fis) throws IOException {
        FileOutputStream outputStream = null;
        try {
            int c;

            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);


            byte[] buffer = new byte[4 * 1024]; // 4 : 4KB buffer
            long total = 0;
            while ((c = fis.read(buffer)) != -1) {
                total += c;
                if (total % 10 == 0) {

                }
                outputStream.write(buffer, 0, c);
            }

        } finally {
            if(fis != null) {
                fis.close();
            }
            if(outputStream != null) {
                outputStream.close();
            }
        }
    }

    public void showFiles(int i){
        InputStream in = null;
        try {
            in = openFileInput("img"+i+".png");

            Bitmap bm = BitmapFactory.decodeStream(in);

            if(bm!=null){
                image.setImageBitmap(bm);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}
